﻿using UnityEngine;
using System.Collections;

public enum GravityDirection
{
    Down,
    Right,
    Up,
    Left
}

public enum AbilityType
{
    [Ability( typeof( DoubleJumpAbility ) )]
    DoubleJump,

    [Ability( typeof( BounceAbility ))]
    Bounce
}
