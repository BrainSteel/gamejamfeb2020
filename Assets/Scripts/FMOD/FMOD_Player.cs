﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMOD_Player : MonoBehaviour
{
   
    void PlayFoley ()

    {
        FMOD.Studio.EventInstance Foley = FMODUnity.RuntimeManager.CreateInstance("event:/Player/Foley");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Foley, transform, GetComponent<Rigidbody2D>());
        Foley.start();
        Foley.release();

    }

    void PlayFootsteps ()

    {
        FMOD.Studio.EventInstance Footsteps = FMODUnity.RuntimeManager.CreateInstance("event:/Player/Footsteps");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Footsteps, transform, GetComponent<Rigidbody2D>());
        Footsteps.start();
        Footsteps.release();
    }

    void PlayJumping ()

    {
        FMOD.Studio.EventInstance Jumping = FMODUnity.RuntimeManager.CreateInstance("event:/Player/Jumping");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Jumping, transform, GetComponent<Rigidbody2D>());
        Jumping.start();
        Jumping.release();
    }

    void PlayLanding ()

    {
        FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance("event:/Player/Landing");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Landing, transform, GetComponent<Rigidbody2D>());
        Landing.start();
        Landing.release();
    }
}
