﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMOD_Dark_Ambience : MonoBehaviour

{
    [FMODUnity.EventRef]
    public string eventPath;
    FMOD.Studio.EventInstance DarkAmbience;

    void Start()
    {
        DarkAmbience = FMODUnity.RuntimeManager.CreateInstance(eventPath);    
    }

    void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        DarkAmbience.start();
 
    }

    void OnDestroy()
    {
        DarkAmbience.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        DarkAmbience.release();
    }

}
