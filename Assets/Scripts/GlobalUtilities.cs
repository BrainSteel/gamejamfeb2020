﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Linq;

public static class GlobalUtilities
{
    private static Scene _last_scene;
    private static GameObject[] _object_cache;

    private static void RefreshCache()
    {
        Scene active = SceneManager.GetActiveScene();
        if( active != _last_scene )
        {
            _last_scene = active;
            _object_cache = active.GetRootGameObjects();
        }
    }

    public static GameObject GetPlayer()
    {
        RefreshCache();
        return _object_cache.Single( obj => obj.TryGetComponent<PlayerControl>( out _ ) );
    }

    public static GameObject GetGameSpace()
    {
        RefreshCache();
        return _object_cache.Single( obj => obj.TryGetComponent<GameSpaceController>( out _ ) );
    }

    public static GameObject GetCamera()
    {
        RefreshCache();
        return _object_cache.Single( obj => obj.TryGetComponent<CameraController>( out _ ) );
    }
}
