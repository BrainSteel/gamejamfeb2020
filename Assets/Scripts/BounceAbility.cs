﻿using UnityEngine;
using UnityEditor;

public class BounceAbility : IPlayerAbility
{
    public const float BounceCooldown = 0.05f;
    public const float BounceFactor = 0.9f;

    private bool _bounce_possible = false;
    private Vector2 _bounce_velocity;
    private bool _subscribed = false;
    private float _bounce_counter = 0.0f;

    public void Update( PlayerControl player )
    {
        if (!_subscribed)
        {
            player.OnSolidCollision += DetectCollision;
            _subscribed = true;
        }

        if ( _bounce_possible )
        {
            if( _bounce_counter <= 0.0f && Input.GetKey( KeyCode.LeftShift ) )
            {
                if( player.PerformMovementAbility( _bounce_velocity, true ) )
                {
                    _bounce_counter = BounceCooldown;
                }
            }
            _bounce_possible = false;
        }

        if ( _bounce_counter > 0.0f )
        {
            _bounce_counter -= Mathf.Min( _bounce_counter, Time.deltaTime );
        }
    }

    private void DetectCollision( PlayerControl player, Vector2 obj_direction, Vector2 velocity )
    {
        // Just take the largest velocity from this frame?
        float largest_cur;
        if( _bounce_possible )
        {
            largest_cur = Mathf.Max( Mathf.Abs( _bounce_velocity.x ), Mathf.Abs( _bounce_velocity.y ) );
        }
        else
        {
            largest_cur = 0.0f;
        }

        float largest_new = Mathf.Max( Mathf.Abs( velocity.x * obj_direction.x ), Mathf.Abs( velocity.y * obj_direction.y ) );
        if( BounceFactor * largest_new > largest_cur )
        {
            Vector2 this_bounce;
            if ( largest_new == Mathf.Abs( velocity.x ) )
            {
                this_bounce = new Vector2( BounceFactor * velocity.x, 0.0f );
            }
            else
            {
                this_bounce = new Vector2( 0.0f, BounceFactor * velocity.y );
            }
            _bounce_velocity = this_bounce;
        }
        
        _bounce_possible = true;
    }
}