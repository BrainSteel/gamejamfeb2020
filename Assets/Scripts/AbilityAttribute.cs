﻿using UnityEngine;
using System.Collections;
using System;

[AttributeUsage( AttributeTargets.Field, AllowMultiple = false, Inherited = false )]
public class AbilityAttribute : Attribute
{
    public Type AbilityType { get; }

    public AbilityAttribute( Type ability_type )
    {
        AbilityType = ability_type;
    }
}
