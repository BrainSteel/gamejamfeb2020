﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpaceController : MonoBehaviour
{
    public static float SwapTimeout = 60.0f;
    public static float RoomSpeed = 30.0f;
    public static float Room1Depth = -20.0f;
    public static float Room2Depth = -10.0f;

    Grid _grid;
    GameObject[] _rooms;
    CameraController _game_camera;
    PlayerControl _player;
    Vector2Int _offset;
    int _width, _height;

    float _zoom_time = 0.0f;
    float _room_time = 0.0f;
    float _swap_timeout = 0;
    GameObject _room1 = null;
    GameObject _room2 = null;
    Vector2 _room1_target;
    Vector2 _room2_target;

    IDictionary<string, SwapSet<SwitchController>> _swaps = new Dictionary<string, SwapSet<SwitchController>>();

    public float RoomWidth => _grid.cellSize.x;
    public float RoomHeight => _grid.cellSize.y;

    // Start is called before the first frame update
    void Start()
    {
        _game_camera = GlobalUtilities.GetCamera().GetComponent<CameraController>();
        _player = GlobalUtilities.GetPlayer().GetComponent<PlayerControl>();
        _grid = GetComponent<Grid>();
        int min_x = int.MaxValue, min_y = int.MaxValue;
        int max_x = int.MinValue, max_y = int.MinValue;
        int total_children = transform.childCount;
        Vector2Int[] locations = new Vector2Int[total_children];
        GameObject[] children = new GameObject[total_children];
        for (int i = 0; i < total_children; i++ )
        {
            Transform child = transform.GetChild( i );
            locations[i] = _grid.WorldToCell( child.position ).To2D();
            children[i] = child.gameObject;
            if( locations[i].x < min_x )
            {
                min_x = locations[i].x;
            }
            
            if ( locations[i].x > max_x )
            {
                max_x = locations[i].x;
            }

            if( locations[i].y < min_y )
            {
                min_y = locations[i].y;
            }
            
            if ( locations[i].y > max_y )
            {
                max_y = locations[i].y;
            }
        }

        _offset.x = -min_x;
        _offset.y = -min_y;
        _width = max_x - min_x + 1;
        _height = max_y - min_y + 1;

        _rooms = new GameObject[_width * _height];
        for( int i = 0; i < total_children; i++ )
        {
            Vector2Int loc = locations[i] + _offset;
            _rooms[loc.x + loc.y * _width] = children[i];
            for (int j = 0; j < children[i].transform.childCount; j++ )
            {
                if( children[i].transform.GetChild( j ).gameObject.TryGetComponent( out SwitchController new_switch ) )
                {
                    SwapSet<SwitchController> set;
                    if( _swaps.TryGetValue( new_switch.SwitchName, out set ) )
                    {
                        set.Add( new_switch );
                    }
                    else
                    {
                        set = new SwapSet<SwitchController>();
                        set.Add( new_switch );
                        _swaps.Add( new_switch.SwitchName, set );
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        GameObject current_room = GetRoom( _player.transform.position.To2D() );
        current_room.GetComponent<RoomController>().Discover();

        if( _swap_timeout > 0.0f )
        {
            if ( _swap_timeout > _zoom_time + _room_time )
            {
                // First stage of room swap
                float dist_1 = Room1Depth - _room1.transform.position.z;
                _room1.transform.position += new Vector3( 0, 0, Mathf.Sign( dist_1 ) * Mathf.Min( Mathf.Abs( dist_1 ), RoomSpeed * Time.deltaTime ) );

                float dist_2 = Room2Depth - _room2.transform.position.z;
                _room2.transform.position += new Vector3( 0, 0, Mathf.Sign( dist_2 ) * Mathf.Min( Mathf.Abs( dist_2 ), RoomSpeed * Time.deltaTime ) );
            }
            else if ( _swap_timeout > _zoom_time )
            {
                // Second stage of room swap
                Vector2 diff1 = _room1_target - _room1.transform.position.To2D();
                float dist1 = diff1.magnitude;
                Vector2 dir1 = diff1.normalized;
                _room1.transform.position += (dir1 * Mathf.Min( dist1, RoomSpeed * Time.deltaTime )).To3D();

                Vector2 diff2 = _room2_target - _room2.transform.position.To2D();
                float dist2 = diff2.magnitude;
                Vector2 dir2 = diff2.normalized;
                _room2.transform.position += (dir2 * Mathf.Min( dist2, RoomSpeed * Time.deltaTime )).To3D();
            }
            else
            {
                // Final stage of room swap
                _game_camera.ClearOverride();
                float dist_1 = 0.0f - _room1.transform.position.z;
                _room1.transform.position += new Vector3( 0, 0, Mathf.Sign( dist_1 ) * Mathf.Min( Mathf.Abs( dist_1 ), RoomSpeed * Time.deltaTime ) );

                float dist_2 = 0.0f - _room2.transform.position.z;
                _room2.transform.position += new Vector3( 0, 0, Mathf.Sign( dist_2 ) * Mathf.Min( Mathf.Abs( dist_2 ), RoomSpeed * Time.deltaTime ) );
            }

            _swap_timeout -= Time.deltaTime;
            if( _swap_timeout <= 0.0f )
            {
                _room1.transform.position = _room1_target.To3D();
                _room2.transform.position = _room2_target.To3D();
                _player.ClearOverride();
                _swap_timeout = 0.0f;
            }
        }
    }

    public void TriggerSwap( SwitchController swap )
    {
        SwapSet<SwitchController> set;
        if ( _swap_timeout == 0.0f && _swaps.TryGetValue( swap.SwitchName, out set ) )
        {
            SwitchController with = set.GetMatch( swap );
            
            _room1 = swap.gameObject.transform.parent.gameObject;
            _room2 = with.gameObject.transform.parent.gameObject;
            _room1_target = _room2.transform.position;
            _room2_target = _room1.transform.position;
            Vector2 room1_pos = _room1.transform.position.To2D() + new Vector2( RoomWidth / 2.0f, RoomHeight / 2.0f );
            Vector2 room2_pos = _room2.transform.position.To2D() + new Vector2( RoomWidth / 2.0f, RoomHeight / 2.0f );
            Vector2 room_center = (room1_pos + room2_pos) / 2.0f;
            Vector2 room_diff = room1_pos - room2_pos;
            float room_len = room_diff.magnitude;
            float camera_size;
            float camera_size_ratio;
            if ( Mathf.Abs( room_diff.x ) > Mathf.Abs( room_diff.y ) )
            {
                camera_size_ratio = _game_camera.CameraWidthRatio;
                camera_size = room_len + RoomWidth;
            }
            else
            {
                camera_size_ratio = _game_camera.CameraHeightRatio;
                camera_size = room_len + RoomHeight;
            }

            Vector3 camera_override = new Vector3( room_center.x, room_center.y, -camera_size / (2.0f * camera_size_ratio) );
            float camera_len = (camera_override - _game_camera.transform.position).magnitude;
            _game_camera.OverridePosition( camera_override );
            GameObject player_room = GetRoom( _player.transform.position.To2D() );
            _player.OverridePosition( (_player.transform.position - player_room.transform.position).To2D(), player_room );
            _zoom_time = Mathf.Max( camera_len / _game_camera.CameraSpeed, Mathf.Abs( Room1Depth ) / RoomSpeed, Mathf.Abs( Room2Depth ) / RoomSpeed );
            _room_time = room_len / RoomSpeed;
            _swap_timeout = _zoom_time * 2 + _room_time;

            // Now that camera/player position are set, it is safe to swap the rooms logically (in cell space)
            SwapRooms( GetCell( _room1.transform.position.To2D() ), GetCell( _room2.transform.position.To2D() ) );
        }
    }

    public Vector2Int GetCell( Vector2 point )
    {
        return _grid.WorldToCell( point.To3D() ).To2D() + _offset;
    }

    public Vector2 GetCenterOfCell( Vector2 point )
    {
        return _grid.GetCellCenterWorld( GetCell( point ).To3D() ).To2D();
    }

    private void SwapRooms( Vector2Int cell1, Vector2Int cell2 )
    {
        int i1 = cell1.x + cell1.y * _width;
        int i2 = cell2.x + cell2.y * _width;
        GameObject room1 = _rooms[i1];
        _rooms[i1] = _rooms[i2];
        _rooms[i2] = room1;
    }

    public GameObject GetRoom( Vector2 point )
    {
        Vector2Int loc = GetCell( point );
        return _rooms[loc.x + loc.y * _width];
    }

    public GameObject GetRoom( GameObject obj )
    {
        return GetRoom( obj.transform.position.To2D() );
    }

    public RoomController GetRoomController( Vector2 point )
    {
        return GetRoom( point ).GetComponent<RoomController>();
    }

    public RoomController GetRoomController( GameObject obj )
    {
        return GetRoom( obj ).GetComponent<RoomController>();
    }
}
