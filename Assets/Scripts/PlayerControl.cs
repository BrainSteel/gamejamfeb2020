﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public const float air_acceleration = 50.0f;
    public const float air_resistance = 0.025f;
    public const float ground_acceleration = 250.0f;
    public const float ground_friction = 0.2f;
    public const float jump_velocity = 15.0f;
    public const float max_horizontal_velocity = 5.0f;
    public const float movement_threshold = 0.1f;
    public const float jump_post_time = 0.08f;
    public const float gravity_hold_time = 0.1f;
    public const float death_duration = 0.94545f;
    public const float attack_duration = 0.5f;
    public const float attack_cooldown = 1.0f;

    private Animator _animator;
    private BoxCollider2D _collider;
    private Rigidbody2D _rigidbody;
    private SpriteRenderer _renderer;
    private RaycastHit2D[] _ray = new RaycastHit2D[1];
    private float _ray_fudge = 1.25f;
    private float _width;
    private float _height;
    private bool _on_ground = false;
    private bool _on_wall = false;
    private float _ground_countdown = 0.0f;
    private float _death_countdown = 0.0f;
    private float _attack_countdown = 0.0f;
    private Vector2 _spawn_position;

    private bool _locked;
    private GameObject _lock_object;
    private Vector2 _lock_position;

    private Vector2 _ability_vel;

    private List<IPlayerAbility> _abilities = new List<IPlayerAbility>();

    private GameSpaceController _game_space;

    public Rigidbody2D RigidBody => _rigidbody;
    public bool OnGround => _on_ground;
    public bool OnWall => _on_wall;

    private GravityDirection _new_gravity;
    private GravityDirection _last_gravity;
    private float _last_gravity_change = -gravity_hold_time;

    public delegate void SolidCollisionEvent( PlayerControl player, Vector2 obj_direction, Vector2 velocity );
    public event SolidCollisionEvent OnSolidCollision;

    public bool Attacking => _attack_countdown > 0.0f;

    public GravityDirection CurrentGravity
    {
        get
        {
            if (Time.time - _last_gravity_change > gravity_hold_time)
            {
                return _new_gravity;
            }
            else
            {
                return _last_gravity;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _collider = GetComponent<BoxCollider2D>();
        _width = _collider.size.x;
        _height = _collider.size.y;
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.gravityScale = 0.0f;
        _rigidbody.drag = 0.0f;
        _rigidbody.sharedMaterial = new PhysicsMaterial2D();
        _rigidbody.sharedMaterial.friction = 0.0f;
        _rigidbody.sharedMaterial.bounciness = 0.0f;
        _rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        _renderer = GetComponent<SpriteRenderer>();
        _game_space = GlobalUtilities.GetGameSpace().GetComponent<GameSpaceController>();
        _spawn_position = transform.position.To2D();
        AddAbility( new JumpAbility() );
    }

    // Update is called once per frame
    void Update()
    {
        if (_death_countdown > 0.0f)
        {
            _death_countdown -= Time.deltaTime;
            if ( _death_countdown <= 0.0f )
            {
                transform.position = _spawn_position;
                _animator.SetBool( "Death", false );
                return;
            }
        }

        if (_locked)
        {
            transform.position = _lock_object.transform.position + _lock_position.To3D();
            return;
        }

        var current_room = _game_space.GetRoomController( gameObject );
        if ( current_room.GravityDirection != _new_gravity )
        {
            _last_gravity = _new_gravity;
            _new_gravity = current_room.GravityDirection;
            _last_gravity_change = Time.time;
        }

        _rigidbody.SetRotation( current_room.GravityRotation );

        if (_ability_vel != new Vector2())
        {
            // During an ability, the player is not on the ground
            _on_ground = false;
        }

        if (_on_ground)
        {
            _ground_countdown = jump_post_time;
        }

        // To be independent of room gravity, rotate the current velocity such that
        // it is as if gravity were down.
        Vector2 current_vel = GravityUtilities.ConvertGravity( _rigidbody.velocity, CurrentGravity, GravityDirection.Down );
        if (_on_ground && current_vel.y < 0.0f)
        {
            current_vel.y = 0.0f;
        }

        Vector2 delta_v;
        if( !_on_ground )
        {
            delta_v = new Vector2( 0.0f, -GravityUtilities.gravity_accel * Time.deltaTime );
        }
        else
        {
            delta_v = new Vector2( 0.0f, 0.0f );
        }

        // Horizontal movement handling
        float horiz_in = Input.GetAxis( "Horizontal" );
        if (_on_ground)
        {
            delta_v.x = horiz_in * ground_acceleration * Time.deltaTime;
            if( Mathf.Abs( current_vel.x ) > movement_threshold )
            {
                delta_v.x -= ground_friction * current_vel.x;
            }
            else
            {
                // Eliminate tiny velocities due to friction
                delta_v.x -= current_vel.x;
            }
        }
        else
        {
            delta_v.x = horiz_in * air_acceleration * Time.deltaTime;
            delta_v.x -= air_resistance * current_vel.x;
        }

        if (Mathf.Abs( current_vel.x ) > max_horizontal_velocity)
        {
            // Only apply delta if it will slow us down
            // NOTE: this allows us to maintain velocities higher than maximum,
            // but not to attain this velocity through running alone (or to increase it)
            if (Mathf.Sign( current_vel.x ) == Mathf.Sign( delta_v.x ))
            {
                delta_v.x = 0.0f;
            }
        }
        else if ( current_vel.x + delta_v.x > max_horizontal_velocity)
        {
            delta_v.x = max_horizontal_velocity - current_vel.x;
        }
        else if ( current_vel.x + delta_v.x < -max_horizontal_velocity )
        {
            delta_v.x = -max_horizontal_velocity - current_vel.x;
        }

        // Apply acceleration due to movement
        current_vel += delta_v;

        // Rendering and Animation
        if ( current_vel.y > -movement_threshold)
        {
            _on_wall = false;
        }

        if( current_vel.x > movement_threshold && !_renderer.flipX )
        {
            _renderer.flipX = true;
        }
        else if( current_vel.x < -movement_threshold && _renderer.flipX )
        {
            _renderer.flipX = false;
        }

        // Rotate the velocity so that it aligns with player gravity
        _rigidbody.velocity = GravityUtilities.ConvertGravity( current_vel, GravityDirection.Down, CurrentGravity );
        _rigidbody.velocity += _ability_vel;
        _ability_vel = new Vector2( 0.0f, 0.0f );

        if( _attack_countdown > 0.0f )
        {
            _animator.SetBool( "Attacking", true );
        }
        else
        {
            _animator.SetBool( "Attacking", false );
            if( _attack_countdown < -attack_cooldown && Input.GetKey( KeyCode.LeftAlt ) )
            {
                _attack_countdown = attack_duration;
            }
        }
        _attack_countdown -= Time.deltaTime;

        if(_on_ground)
        {
            _animator.SetBool( "OnGround", true );
            _animator.SetBool( "OnWall", false );
            if( Mathf.Abs( current_vel.x ) > movement_threshold )
            {
                _animator.SetBool( "Running", true );
            }
            else
            {
                _animator.SetBool( "Running", false );
            }
        }
        else
        {
            _animator.SetBool( "OnGround", false );
            if( _on_wall )
            {
                _animator.SetBool( "OnWall", true );
            }
            else
            {
                _animator.SetBool( "OnWall", false );
                if( current_vel.y >= 0.0f )
                {
                    _animator.SetBool( "Falling", false );
                }
                else
                {
                    _animator.SetBool( "Falling", true );
                }
            }
        }

        if ( _ground_countdown > 0 ) _ground_countdown -= Mathf.Min( Time.deltaTime, _ground_countdown );

        foreach( var ability in _abilities )
        {
            ability.Update( this );
        }
    }

    void OnCollisionEnter2D( Collision2D collision )
    {
        if( collision != null )
        {
            Vector2 dir = new Vector2();
            if( _collider.Raycast( GravityUtilities.GetDownDirection( CurrentGravity ), _ray, _ray_fudge * _height / 2 ) > 0 )
            {
                dir = GravityUtilities.GetDownDirection( CurrentGravity );
                _on_ground = true;
                _on_wall = false;
            }
            else if( _collider.Raycast( GravityUtilities.GetUpDirection( CurrentGravity ), _ray, _ray_fudge * _height / 2 ) > 0 )
            {
                dir = GravityUtilities.GetUpDirection( CurrentGravity );
            }

            if( !_on_ground )
            {
                Vector2 rel_vel = GravityUtilities.ConvertGravity( _rigidbody.velocity, CurrentGravity, GravityDirection.Down );
                if( rel_vel.y < 0 )
                {
                    if( _collider.Raycast( GravityUtilities.GetLeftDirection( CurrentGravity ), _ray, _ray_fudge * _width / 2 ) > 0 )
                    {
                        dir = GravityUtilities.GetLeftDirection( CurrentGravity );
                        _on_wall = true;
                    }
                    else if( _collider.Raycast( GravityUtilities.GetRightDirection( CurrentGravity ), _ray, _ray_fudge * _width / 2 ) > 0 )
                    {
                        dir = GravityUtilities.GetRightDirection( CurrentGravity );
                        _on_wall = true;
                    }
                }
            }

            if( dir != new Vector2() )
            {
                OnSolidCollision?.Invoke( this, dir, collision.relativeVelocity );
            }
        }
    }

    void OnCollisionStay2D( Collision2D collision )
    {
        if( _collider.Raycast( GravityUtilities.GetDownDirection( CurrentGravity ), _ray, _ray_fudge * _height / 2 ) > 0 )
        {
            _on_ground = true;
            _on_wall = false;
        }

        if( !_on_ground )
        {
            Vector2 rel_vel = GravityUtilities.ConvertGravity( _rigidbody.velocity, CurrentGravity, GravityDirection.Down );
            if( rel_vel.y < 0 )
            {
                if( _collider.Raycast( GravityUtilities.GetLeftDirection( CurrentGravity ), _ray, _ray_fudge * _width / 2 ) > 0 )
                {
                    _on_wall = true;
                }
                else if( _collider.Raycast( GravityUtilities.GetRightDirection( CurrentGravity ), _ray, _ray_fudge * _width / 2 ) > 0 )
                {
                    _on_wall = true;
                }
            }
        }
    }

    private void OnCollisionExit2D( Collision2D collision )
    {
        _on_ground = false;
        _on_wall = false;
    }

    public void OverridePosition( Vector2 relative, GameObject to )
    {
        _locked = true;
        _lock_object = to;
        _lock_position = relative;
    }

    public void ClearOverride()
    {
        _locked = false;

        // Clear velocity when switching rooms
        _rigidbody.velocity = new Vector2( 0.0f, 0.0f );
    }

    public Vector2 GetJump()
    {
        return GravityUtilities.GetUpDirection( CurrentGravity ) * jump_velocity;
    }

    public void AddAbility( IPlayerAbility ability )
    {
        _abilities.Add( ability );
    }

    public bool PerformMovementAbility( Vector2 velocity, bool require_ground )
    {
        if( !require_ground || _ground_countdown > 0.0f )
        {
            _ground_countdown = 0.0f;
            if( velocity.magnitude > _ability_vel.magnitude )
            {
                _ability_vel = velocity;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Death( )
    {
        _animator.SetBool( "Death", true );
        _death_countdown = death_duration;
    }
}
