﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class RoomController : MonoBehaviour
{
    public GravityDirection GravityDirection;

    GameSpaceController _game_space;

    public Vector2 Gravity => GravityUtilities.GetGravity( GravityDirection );

    public Quaternion GravityRotation => GravityUtilities.GetGravityRotation( GravityDirection );

    // Start is called before the first frame update
    void Start()
    {
        var game_space_object = GlobalUtilities.GetGameSpace();
        _game_space = game_space_object.GetComponent<GameSpaceController>();
        ChangeVisibility( false );
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector2 GetCenter()
    {
        return transform.position.To2D() + new Vector2( _game_space.RoomWidth / 2, _game_space.RoomHeight / 2 );
    }

    private void ChangeVisibility( bool visible )
    {
        for( int i = 0; i < gameObject.transform.childCount; i++ )
        {
            GameObject obj = gameObject.transform.GetChild( i ).gameObject;
            if( obj.TryGetComponent<Renderer>( out Renderer r ) )
            {
                if( obj.name == "Fog" )
                {
                    r.enabled = !visible;
                }
                else
                {
                    r.enabled = visible;
                }
            }
        }
    }

    public void Discover()
    {
        ChangeVisibility( true );
    }
}
