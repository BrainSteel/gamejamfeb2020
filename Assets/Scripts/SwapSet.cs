﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwapSet<T> where T : class
{
    List<T> _swaps = new List<T>();

    public void Add( T swap )
    {
        _swaps.Add( swap );
    }

    public T GetMatch( T other )
    {
        for (int i = 0; i < _swaps.Count - 1; i++ )
        {
            if ( _swaps[i] == other )
            {
                return _swaps[i + 1];
            }
        }
        return _swaps[0];
    }
}
