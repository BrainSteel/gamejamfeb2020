﻿using UnityEngine;
using System.Collections;

public static class GravityUtilities
{
    public const float gravity_accel = 25;

    public static Vector2 GetGravity( GravityDirection gravity_dir )
    {
        return GetDownDirection( gravity_dir ) * gravity_accel;
    }

    public static Quaternion GetGravityRotation( GravityDirection gravity_dir )
    {
        Vector3 axis = new Vector3( 0.0f, 0.0f, 1.0f );
        switch( gravity_dir )
        {
            case GravityDirection.Down:
                return Quaternion.identity;
            case GravityDirection.Up:
                return Quaternion.AngleAxis( 180, axis );
            case GravityDirection.Left:
                return Quaternion.AngleAxis( 270, axis );
            case GravityDirection.Right:
                return Quaternion.AngleAxis( 90, axis );
            default:
                return Quaternion.identity;
        }
    }

    private static Vector2 RotateToGravity( Vector2 vect, GravityDirection gravity_dir )
    {
        switch( gravity_dir )
        {
            case GravityDirection.Down:
                return vect;
            case GravityDirection.Up:
                return new Vector2( -vect.x, -vect.y );
            case GravityDirection.Left:
                return new Vector2( vect.y, -vect.x );
            case GravityDirection.Right:
                return new Vector2( -vect.y, vect.x );
            default:
                return vect;
        }
    }

    private static Vector2 RotateFromGravity( Vector2 vect, GravityDirection gravity_dir )
    {
        switch( gravity_dir )
        {
            case GravityDirection.Down:
                return vect;
            case GravityDirection.Up:
                return new Vector2( -vect.x, -vect.y );
            case GravityDirection.Left:
                return new Vector2( -vect.y, vect.x );
            case GravityDirection.Right:
                return new Vector2( vect.y, -vect.x );
            default:
                return vect;
        }
    }

    public static Vector2 GetDownDirection( GravityDirection gravity_dir )
    {
        return RotateToGravity( new Vector2( 0.0f, -1.0f ), gravity_dir );
    }

    public static Vector2 GetUpDirection( GravityDirection gravity_dir )
    {
        return RotateToGravity( new Vector2( 0.0f, 1.0f ), gravity_dir );
    }

    public static Vector2 GetRightDirection( GravityDirection gravity_dir )
    {
        return RotateToGravity( new Vector2( 1.0f, 0.0f ), gravity_dir );
    }

    public static Vector2 GetLeftDirection( GravityDirection gravity_dir )
    {
        return RotateToGravity( new Vector2( -1.0f, 0.0f ), gravity_dir );
    }

    public static Vector2 ConvertGravity( Vector2 vect, GravityDirection from, GravityDirection to )
    {
        // I hope the compiler is smart enough to inline this.
        return RotateToGravity( RotateFromGravity( vect, from ), to );
    }
}
