﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;
using UnityEngine;

public class AbilityController : MonoBehaviour
{
    public GameObject ActivateAmbience;
    public GameObject ActivateDestroyAmbience;

    public AbilityType Ability;

    private IPlayerAbility _constructed_ability;
    private PlayerControl _player_control;
    private Collider2D _player_collider;
    private Collider2D _this_collider;

    // Start is called before the first frame update
    void Start()
    {
        var attrib = typeof( AbilityType ).GetMember( Ability.ToString() )[0].GetCustomAttribute<AbilityAttribute>();
        _constructed_ability = Activator.CreateInstance( attrib.AbilityType ) as IPlayerAbility;
        var player_obj = GlobalUtilities.GetPlayer();
        _player_control = player_obj.GetComponent<PlayerControl>();
        _player_collider = player_obj.GetComponent<Collider2D>();
        _this_collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if( _this_collider.IsTouching( _player_collider ) )
        {
            gameObject.SetActive( false );
            _player_control.AddAbility( _constructed_ability );
            if( ActivateAmbience != null )
            {
                ActivateAmbience.SetActive( true );
            }

            if( ActivateDestroyAmbience != null )
            {
                ActivateDestroyAmbience.SetActive( true );
            }
        }
    }
}
