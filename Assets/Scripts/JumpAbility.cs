﻿using UnityEngine;
using System.Collections;

public class JumpAbility : IPlayerAbility
{
    public const float jump_pre_time = 0.08f;
    private float _jump_countdown = 0;

    // Update is called once per frame
    public void Update( PlayerControl player )
    {
        if( Input.GetButtonDown( "Jump" ) )
        {
            _jump_countdown = jump_pre_time;
        }

        if (_jump_countdown > 0.0f)
        {
            if ( player.PerformMovementAbility( player.GetJump(), true ) )
            {
                _jump_countdown = 0.0f;
            }
        }

        _jump_countdown -= Mathf.Min( Time.deltaTime, _jump_countdown );
    }
}
