﻿using UnityEngine;
using System.Collections;

public class DoubleJumpAbility : IPlayerAbility
{
    private bool _second_used = false;
    private float _last_ground;

    public void Update( PlayerControl player )
    {
        Vector2 rel_vel = GravityUtilities.ConvertGravity( player.RigidBody.velocity, player.CurrentGravity, GravityDirection.Down );
        if( player.OnGround )
        {
            _second_used = false;
            _last_ground = Time.time;
        }
        else if( !_second_used && Input.GetButtonDown( "Jump" ) && Time.time - _last_ground > PlayerControl.jump_post_time )
        {
            rel_vel = new Vector2( 0.0f, PlayerControl.jump_velocity - rel_vel.y);
            Vector2 total_vel = GravityUtilities.ConvertGravity( rel_vel, GravityDirection.Down, player.CurrentGravity );
            if( player.PerformMovementAbility( total_vel, false ) )
            {
                _second_used = true;
            }
        }
    }
}
