﻿using UnityEngine;
using System.Collections;

public static class VectorExtensions
{
    public static Vector2 To2D(this Vector3 vector)
    {
        return new Vector2( vector.x, vector.y );
    }

    public static Vector2Int To2D(this Vector3Int vector )
    {
        return new Vector2Int( vector.x, vector.y );
    }

    public static Vector3 To3D( this Vector2 vector )
    {
        return new Vector3( vector.x, vector.y, 0.0f );
    }

    public static Vector3Int To3D( this Vector2Int vector )
    {
        return new Vector3Int( vector.x, vector.y, 0 );
    }
}
