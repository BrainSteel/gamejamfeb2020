﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        body.gravityScale = 0.0f;
    }

    private void OnCollisionEnter2D( Collision2D collision )
    {
        if( collision != null )
        {
            if( collision.gameObject.TryGetComponent<PlayerControl>( out var player ) )
            {
                player.Death();
            }
            GameObject.Destroy( gameObject );
        }
    }
}
