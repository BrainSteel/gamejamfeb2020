﻿using UnityEngine;
using System.Collections;

public interface IPlayerAbility
{
    void Update( PlayerControl player );
}
