﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderController : MonoBehaviour
{
    public const float AttackDuration = 1.0f;
    public const float AttackCooldown = 2.0f;
    public const float AttackSpeed = 30.0f;
    public const float AttackAngle = Mathf.PI / 8;
    public const float DeathTime = 2.0f;
    public const float GroundFriction = 0.3f;
    public readonly Vector2 AttackRange = new Vector2( 10.0f, 5.0f );

    private Animator _animator;
    private BoxCollider2D _collider;
    private Rigidbody2D _rigidbody;
    private SpriteRenderer _renderer;
    private GameSpaceController _game_space;
    private PlayerControl _player;

    private GravityDirection _current_gravity;
    private RaycastHit2D[] _ray = new RaycastHit2D[1];
    private float _height;
    private float _ray_fudge = 1.2f;
    private float _attack_counter = -AttackCooldown;
    private bool _on_ground = false;
    private float _dead_counter = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _collider = GetComponent<BoxCollider2D>();
        _height = _collider.size.y;
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.gravityScale = 0.0f;
        _rigidbody.drag = 0.0f;
        _rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        _renderer = GetComponent<SpriteRenderer>();
        _game_space = GlobalUtilities.GetGameSpace().GetComponent<GameSpaceController>();
        _player = GlobalUtilities.GetPlayer().GetComponent<PlayerControl>();
    }

    // Update is called once per frame
    void Update()
    {
        RoomController cur_room = _game_space.GetRoomController( gameObject );
        _current_gravity = cur_room.GravityDirection;
        _rigidbody.SetRotation( cur_room.GravityRotation );

        // To be independent of room gravity, rotate the current velocity such that
        // it is as if gravity were down.
        Vector2 current_vel = GravityUtilities.ConvertGravity( _rigidbody.velocity, _current_gravity, GravityDirection.Down );
        if( _on_ground && current_vel.y < 0.0f )
        {
            current_vel.y = 0.0f;
        }

        Vector2 delta_v;
        if( !_on_ground )
        {
            delta_v = new Vector2( 0.0f, -GravityUtilities.gravity_accel * Time.deltaTime );
        }
        else
        {
            delta_v = new Vector2( -current_vel.x * GroundFriction, 0.0f );
        }

        current_vel += delta_v;
        _rigidbody.velocity = GravityUtilities.ConvertGravity( current_vel, GravityDirection.Down, _current_gravity );

        if (current_vel.x > 0.0f)
        {
            _renderer.flipX = true;
        }
        else if ( current_vel.x < 0.0f )
        {
            _renderer.flipX = false;
        }

        Vector2 player_diff = _player.transform.position - transform.position;
        player_diff = GravityUtilities.ConvertGravity( player_diff, _current_gravity, GravityDirection.Down );

        if ( _dead_counter > 0.0f )
        {
            _animator.SetBool( "Death", true );
            if( _dead_counter < Time.deltaTime )
            {
                gameObject.SetActive( false );
            }
            _dead_counter -= Time.deltaTime;
        }
        else if( Mathf.Abs( player_diff.x ) < AttackRange.x && player_diff.y > 0.0f && player_diff.y < AttackRange.y && _attack_counter <= -AttackCooldown )
        {
            Vector2 atk = AttackSpeed * new Vector2( Mathf.Sign( player_diff.x ) * Mathf.Cos( AttackAngle ), Mathf.Sin( AttackAngle ) );
            _rigidbody.velocity += GravityUtilities.ConvertGravity( atk, GravityDirection.Down, _current_gravity );
            _animator.SetBool( "Attacking", true );
            _attack_counter = AttackDuration;
        }
        else if ( _attack_counter <= 0.0f )
        {
            _animator.SetBool( "Attacking", false );
        }

        _attack_counter -= Time.deltaTime;
    }

    private void OnCollisionEnter2D( Collision2D collision )
    {
        if( collision != null && collision.gameObject.TryGetComponent<PlayerControl>( out _ ) )
        {
            if( _player.Attacking )
            {
                _dead_counter = DeathTime;
            }
            else if ( _dead_counter <= 0.0f )
            {
                _player.Death();
            }
        }
        else
        {
            if( _collider.Raycast( GravityUtilities.GetDownDirection( _current_gravity ), _ray, _ray_fudge * _height / 2 ) > 0 )
            {
                _on_ground = true;
            }
        }
    }

    private void OnCollisionExit2D( Collision2D collision )
    {
        _on_ground = false;
    }
}
