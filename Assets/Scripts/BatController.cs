﻿using UnityEngine;
using System.Collections;

public class BatController : MonoBehaviour
{
    
    public const float AttackCooldown = 2.0f;
    public const float FlySpeed = 2.0f;
    public const float FlyDistance = 5.0f;
    public const float RetargetCooldown = 5.0f;
    public const float DeathDuration = 1.0f;
    public const float AttackRange = 20.0f;
    public const float ProjectileSpeed = 3.0f;
    public const float ProjectileSpawnRange = 1.0f;

    public GameObject Projectile;

    private Animator _animator;
    private BoxCollider2D _collider;
    private Rigidbody2D _rigidbody;
    private SpriteRenderer _renderer;
    private GameSpaceController _game_space;
    private PlayerControl _player;

    private float _attack_counter = 0.0f;
    private float _retarget_counter = 0.0f;
    private float _target_angle = 0.0f;
    private float _dead_counter = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.gravityScale = 0.0f;
        _rigidbody.drag = 0.0f;
        _rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        _renderer = GetComponent<SpriteRenderer>();
        _game_space = GlobalUtilities.GetGameSpace().GetComponent<GameSpaceController>();
        _player = GlobalUtilities.GetPlayer().GetComponent<PlayerControl>();
    }

    // Update is called once per frame
    void Update()
    {
        if( _dead_counter > 0.0f )
        {
            _animator.SetBool( "Death", true );
            if( _dead_counter < Time.deltaTime )
            {
                gameObject.SetActive( false );
            }
            _dead_counter -= Time.deltaTime;
            return;
        }

        RoomController cur_room = _game_space.GetRoomController( gameObject );
        var current_gravity = cur_room.GravityDirection;
        _rigidbody.SetRotation( cur_room.GravityRotation );

        Vector2 player_diff = _player.transform.position - transform.position;
        player_diff = GravityUtilities.ConvertGravity( player_diff, current_gravity, GravityDirection.Down );

        if( player_diff.magnitude < AttackRange && _attack_counter <= 0.0f )
        {
            var n = player_diff.normalized;
            var result = GameObject.Instantiate( Projectile );
            result.transform.position = transform.position.To2D() + n * ProjectileSpawnRange;
            var proj_body = result.GetComponent<Rigidbody2D>();
            proj_body.velocity = n * ProjectileSpeed;
            _attack_counter = AttackCooldown;
        }

        if (_retarget_counter <= 0.0f)
        {
            _target_angle = Random.Range( 0, 2 * Mathf.PI );
            _retarget_counter = RetargetCooldown;
        }

        if( _game_space.GetRoom( gameObject ) == _game_space.GetRoom( _player.gameObject ) )
        {
            Vector2 cur_target = _player.transform.position.To2D() + FlyDistance * new Vector2( Mathf.Cos( _target_angle ), Mathf.Sin( _target_angle ) );
            _rigidbody.velocity = (cur_target - transform.position.To2D()).normalized * FlySpeed;

            Vector2 rel_vel = GravityUtilities.ConvertGravity( _rigidbody.velocity, current_gravity, GravityDirection.Down );
            if( rel_vel.x < 0.0f )
            {
                _renderer.flipX = false;
            }
            else if( rel_vel.x > 0.0f )
            {
                _renderer.flipX = true;
            }
        }

        _retarget_counter -= Time.deltaTime;
        _attack_counter -= Time.deltaTime;
    }

    private void OnCollisionEnter2D( Collision2D collision )
    {
        if( collision != null && collision.gameObject.TryGetComponent<PlayerControl>( out _ ) )
        {
            if( _player.Attacking )
            {
                _dead_counter = DeathDuration;
            }
            else if ( _dead_counter <= 0.0f )
            {
                _player.Death();
            }
        }
    }
}
