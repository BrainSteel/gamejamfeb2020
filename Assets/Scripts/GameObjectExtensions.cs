﻿using Unity;
using UnityEngine;

using System;
using System.Collections.Generic;

public static class GameObjectExtensions
{
    public static GameObject FirstChild( this GameObject parent, Func<GameObject, bool> predicate )
    {
        for( int i = 0; i < parent.transform.childCount; i++ )
        {
            var result = parent.transform.GetChild( i ).gameObject;
            if ( predicate( result ) )
            {
                return result;
            }
        }

        return null;
    }
}
