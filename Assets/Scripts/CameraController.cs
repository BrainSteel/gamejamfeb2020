﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float CameraWidthRatio => 11.0f / 10.0f;
    public float CameraHeightRatio => CameraWidthRatio / _camera.aspect;

    public float DefaultDepth => -10.0f;
    public float CameraSpeed = 100.0f;

    GameObject _player;
    GameSpaceController _game_space;
    Camera _camera;
    GameObject _current_room;

    private bool _override = false;
    private Vector3 _commanded_pos;

    // Start is called before the first frame update
    void Start()
    {
        _player = GlobalUtilities.GetPlayer();
        GameObject top = GlobalUtilities.GetGameSpace();
        _game_space = top.GetComponent<GameSpaceController>();
        _camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        float camera_height_ratio = CameraHeightRatio;
        if( !_override )
        {
            _current_room = _game_space.GetRoom( _player.transform.position.To2D() );
            float x_pos = _game_space.RoomWidth / 2 + _current_room.transform.position.x;
            float y_min = _current_room.transform.position.y - camera_height_ratio * transform.position.z;
            float y_max = _current_room.transform.position.y + _game_space.RoomHeight + camera_height_ratio * transform.position.z;
            float y_pos = Mathf.Max( y_min, Mathf.Min( y_max, _player.transform.position.y ) );
            _commanded_pos = new Vector3( x_pos, y_pos, DefaultDepth );
        }

        Vector3 diff = _commanded_pos - transform.position;
        Vector3 vel = diff.normalized * CameraSpeed * Time.deltaTime;
        if ( vel.sqrMagnitude > diff.sqrMagnitude)
        {
            transform.position = _commanded_pos;
        }
        else
        {
            transform.position += vel;
        }

        _camera.orthographicSize = -transform.position.z * camera_height_ratio;
    }

    public void OverridePosition( Vector3 pos )
    {
        _override = true;
        _commanded_pos = pos;
    }

    public void ClearOverride()
    {
        _override = false;
    }
}
