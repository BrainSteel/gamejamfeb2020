﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchController : MonoBehaviour
{
    public string SwitchName = "Undefined";
    public float CooldownTime = 10.0f;

    GameSpaceController _game_space;
    Collider2D _player_collider;
    Collider2D _this_collider;

    float timeout = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        _game_space = GlobalUtilities.GetGameSpace().GetComponent<GameSpaceController>();
        _player_collider = GlobalUtilities.GetPlayer().GetComponent<Collider2D>();
        _this_collider = GetComponent<Collider2D>();
    }

    private void Update()
    {
        if (timeout > 0.0f)
        {
            timeout -= Time.deltaTime;
            if ( timeout < 0.0f )
            {
                timeout = 0.0f;
            }
        }
    }

    void FixedUpdate()
    {
        if ( timeout == 0.0f && _this_collider.IsTouching( _player_collider ) )
        {
            timeout = CooldownTime;
            _game_space.TriggerSwap( this );
            GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
    }
}
